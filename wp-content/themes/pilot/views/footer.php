<footer class="site-footer">
	<div class="site-info">
		<?php echo date("Y"); ?>&copy; Crafti. All Rights Reserved. <a href="/privacy-policy">Privacy Policy</a>. <a href="terms-conditions">Terms &amp; Conditions</a>. <?php printf( esc_html__( 'Design by %1$s', 'pilot' ), '<a href="http://sonderagency.com/">Sonder</a>' ); ?>
	</div><!-- .site-info -->
</footer>
<script src="/wp-content/themes/pilot/dest/js/demo.js"></script>
<script src="/wp-content/themes/pilot/dest/js/three.min.js"></script>
<script src="/wp-content/themes/pilot/dest/js/perlin.js"></script>
<script src="/wp-content/themes/pilot/dest/js/app.min.js"></script>
<script src="/wp-content/themes/pilot/dest/js/TweenMax.min.js"></script>
<script src="/wp-content/themes/pilot/dest/js/demo4.js"></script>